package com.codewhatuwant.trainings.pressuremedicinedoser.needs;

public interface AlertService {

    void notifyDoctor();

}
