package com.codewhatuwant.trainings.pressuremedicinedoser.needs;

public enum Medicine {
    PRESSURE_LOWERING, PRESSURE_RAISING
}
