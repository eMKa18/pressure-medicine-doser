package com.codewhatuwant.trainings.pressuremedicinedoser.needs;

public interface MedicinePump {

    void dose(Medicine medicine);

    int minutesSinceLastDose(Medicine medicine);

}
