package com.codewhatuwant.trainings.pressuremedicinedoser.needs;

public interface PressureMonitor {

    int getSystolicBloodPressure();

}
